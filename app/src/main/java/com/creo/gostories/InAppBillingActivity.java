package com.creo.gostories;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.creo.gostories.util.IabHelper;
import com.creo.gostories.util.IabResult;
import com.creo.gostories.util.Inventory;
import com.creo.gostories.util.Purchase;

import java.util.ArrayList;


public class InAppBillingActivity extends Activity {

    public static final String POINTS_PACK_5000 = "com.sandeep.gostories1.p5000";
    public static final String POINTS_PACK_2000 = "com.sandeep.gostories1.p2000";
    public static final String POINTS_PACK_12000 = "com.sandeep.gostories1.p12000";
    static final int RC_REQUEST = 10001;
    String developerPayload = "NishantDeveloperPayload";
    IabHelper mHelper;
    public final static String TAG = "InAppBilling_LOG_TAG";
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener;
    IabHelper.QueryInventoryFinishedListener mQueryFinishedListener;
    public ListView skuItemsListView;
    ArrayAdapter<String>  iapListArrayAdapter;
    public static ArrayList<String> skuItemsArrFromServer;
    public ArrayList<String> customIAPArrList = new ArrayList<>();
    ProgressDialog progressDialog;
    Toast mToast;
    static public int purchasedPointsBasket = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.in_app_billing_layout);

        customIAPArrList.add("Nishant");
        skuItemsListView = (ListView) findViewById(R.id.skuListView);
        iapListArrayAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, customIAPArrList);

        skuItemsListView.setVisibility(View.INVISIBLE);
        skuItemsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Log.d(TAG, skuItemsArrFromServer.get(position));
                launchPurchaseFlowForSelectedItem(skuItemsArrFromServer.get(position));
            }
        });

//        String appVersionNameStr = ""+BuildConfig.VERSION_CODE;
//        showToastWithMessage("appVersionNameStr: "+appVersionNameStr);
        // Assign adapter to ListView
        skuItemsListView.setAdapter(iapListArrayAdapter);

        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5/BvH/SbQ2Dn7o99NEcGDtTdqYaJPr9OHKNSkV0SEi1Jrqr0HCx0hbTAASDziMBkB3yZ48y5pQ9UfaFlLd1kw6oiJcgUwn7OdOL7JBXqfXn5A+EArja7ZeWfJ1oTCElkuaMctalTar/dRQvUNzljo/47ow5Qwl0KemKMHG9B61z56Ve5v11W3L0NAgCYS4kAhJWvkB+0QfGgdlulnpmzZXAYxd7y3yp2Xb26wahpOmHBc5SLJcdqwCt7MOZyj7HVAcOP5i62W1AgayfSpadJhFmM0dY8/IPAUiADK3KaPbpuDHGqeIbeUN8sX6PG+60jetIvNWXozZzCL/7ldndlSQIDAQAB";
        mHelper = new IabHelper(this, base64EncodedPublicKey);
        mHelper.enableDebugLogging(true);

        if (skuItemsArrFromServer == null) {
            skuItemsArrFromServer = new ArrayList<> ();
            skuItemsArrFromServer.add(POINTS_PACK_2000);
            skuItemsArrFromServer.add(POINTS_PACK_5000);
            skuItemsArrFromServer.add(POINTS_PACK_12000);
        }

        startSetupOfIABHelper();
        setupQueryFinishedListener();
        setupPurchaseFinishedListener();
    }

    public void startSetupOfIABHelper(){
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                // Log.d(TAG, "Setup finished.");

                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    String errMsg = "Basic setup of IAB helper Failed. "+result.getMessage();
                    Log.e(TAG, errMsg);
                    showToastWithMessage(errMsg);
                    return;
                }

                // Have we been disposed of in the meantime? If so, quit.
                if (mHelper == null) return;

                // IAB is fully set up. Now, let's get an inventory of stuff we own.
                //Log.d(TAG, "IabHelper Setup successful..");
                //launchPurchaseFlowForSelectedItem(POINTS_PACK_2000);
            }
        });
    }

    public void setupQueryFinishedListener(){
        mQueryFinishedListener = new IabHelper.QueryInventoryFinishedListener() {
            public void onQueryInventoryFinished(IabResult result, Inventory inventory)
            {
                if (progressDialog != null){
                    progressDialog.dismiss();
                }
                Log.d(TAG, "onQueryInventoryFinished...   inventory: " + inventory);
                if (result.isFailure()) {
                    String errMsg = "Problem querying inventory. "+result.getMessage();
                    Log.e(TAG, errMsg);
                    showToastWithMessage(errMsg);
                    return;
                }

                try
                {
                    customIAPArrList.clear();

                    for (String skuItem : skuItemsArrFromServer) {
                        if (inventory.getSkuDetails(skuItem) != null){
                            String price = inventory.getSkuDetails(skuItem).getPrice();
                            String title = inventory.getSkuDetails(skuItem).getTitle();
                            if (title != null && price != null){
                                String titleWithPrice = title + "  -  " + price;
                                customIAPArrList.add(titleWithPrice);
                            }
                        }
                    }

                    // Log.d(TAG, "customIAPArrList: " + customIAPArrList);
                    skuItemsListView.setVisibility(View.VISIBLE);
                    iapListArrayAdapter.notifyDataSetChanged();

                    // update the UI
                }
                catch (Exception e){
                    e.printStackTrace();
                    String errMsg = "Query Inventory Finishing Failed. "+e.getLocalizedMessage();
                    Log.e(TAG, errMsg);
                    showToastWithMessage(errMsg);
                }
            }
        };
    }

    public void setupPurchaseFinishedListener(){
        // Callback for when a purchase is finished
        mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
            public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
                //Log.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase);

                // if we were disposed of in the meantime, quit.
                if (mHelper == null) return;

                if (result.isFailure()) {
                    String errMsg = "Purchase not processed.\n"+result.getMessage();
                    Log.e(TAG, errMsg);
                    showToastWithMessage(errMsg);
                    return;
                }
                try {
                    String purchasedProductId = purchase.getSku();
                    showToastWithMessage(purchasedProductId);
                    purchasedPointsBasket = purchasedPointsBasket + Integer.parseInt(purchasedProductId);
                }catch (Exception e){
                    String errMsg = "Failed to process purchase "+result.getMessage()+"\n Immediately contact GoStories support team\nfriends@gostories.co.in";
                    Log.e(TAG, errMsg);
                    showToastWithMessage(errMsg);
                }
            }
        };
    }

    public void showToastWithMessage(String msg) {
        mToast = new Toast(InAppBillingActivity.this);
        if (mToast == null){
            mToast = new Toast(InAppBillingActivity.this);
        }else{
            mToast.cancel();
        }
        mToast.makeText(getApplicationContext(),
                msg, Toast.LENGTH_LONG)
                .show();
    }

    public void queryAsync(){
        try{
            mHelper.queryInventoryAsync(true, skuItemsArrFromServer,
                    mQueryFinishedListener);
        }catch (Exception e){
            String errMsg = "Product inventory not available. "+e.getLocalizedMessage();
            Log.e(TAG, errMsg);
            showToastWithMessage(errMsg);

            if (progressDialog != null){
                progressDialog.dismiss();
            }
        }
    }

    public void launchPurchaseFlowForSelectedItem(String selectedItemId){
        try {
            if (mHelper.mAsyncInProgress){mHelper.flagEndAsync();}
            mHelper.launchPurchaseFlow(InAppBillingActivity.this, selectedItemId, RC_REQUEST, mPurchaseFinishedListener, developerPayload);
        }catch (Exception e){
            String errMsg = "Failed to start Purchase for: "+selectedItemId+"    "+e.getLocalizedMessage();
            Log.e(TAG, errMsg);
            showToastWithMessage(errMsg);
            if (progressDialog != null){
                progressDialog.dismiss();
            }
        }
    }

    public void loadIAPItems(View v){
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading IAP items..");
        progressDialog.setCancelable(false);
        progressDialog.show();
        queryAsync();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelper != null) mHelper.dispose();
        mHelper = null;
        purchasedPointsBasket = 0;
    }
}
