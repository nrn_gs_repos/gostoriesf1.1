package com.creo.gostories;

import java.io.Serializable;

/**
 * Created by Nishant on 7/21/2015.
 */
public class StoryListModel implements Serializable {
    private  String storyid="";
    private  String storyname="";
    private  String timeduration="";
    private  String director="";
    private  String authorname="";
    private  String artistname="";

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    private  String authoreid="";
    private  String artistid="";
    private  String bookname="";

    public String getBookname() {
        return bookname;
    }

    public void setBookname(String bookname) {
        this.bookname = bookname;
    }

    public String getStoryid() {
        return storyid;
    }

    public void setStoryid(String storyid) {
        this.storyid = storyid;
    }
    public String getAuthoreid() {
        return authoreid;
    }

    public void setAuthoreid(String authoreid) {
        this.authoreid = authoreid;
    }

    public String getArtistid() {
        return artistid;
    }

    public void setArtistid(String artistid) {
        this.artistid = artistid;
    }


    public String getTimeduration() {
        return timeduration;
    }

    public void setTimeduration(String timeduration) {
        this.timeduration = timeduration;
    }

    public String getArtistname() {
        return artistname;
    }

    public void setArtistname(String artistname) {
        this.artistname = artistname;
    }
    public String getAuthorname() {
        return authorname;
    }

    public void setAuthorname(String authorname) {
        this.authorname = authorname;
    }

    public String getStoryname() {
        return storyname;
    }

    public void setStoryname(String storyname) {
        this.storyname = storyname;
    }


}
