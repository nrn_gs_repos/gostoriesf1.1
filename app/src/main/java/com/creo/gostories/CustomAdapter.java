package com.creo.gostories;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Nishant Thite on 7/21/2015.
 */
public class CustomAdapter  extends BaseAdapter  {

    /*********** Declare Used Variables *********/
    private Activity activity;
    private ArrayList data;
    private static LayoutInflater inflater;

    StoryListModel tempValues=null;
    int i=0;

    /*************  CustomAdapter Constructor *****************/
    public CustomAdapter(Activity activity, ArrayList d) {

        /********** Take passed values **********/
        this.activity = activity;
        this.data=d;


//        /***********  Layout inflator to call external xml layout () ***********/
//        inflater = ( LayoutInflater )activity.
//                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    /******** What is the size of Passed Arraylist Size ************/
    public int getCount() {

        if(data.size()<0)
            return 1;
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    /********* Create a holder Class to contain inflated xml file elements *********/
    public static class ViewHolder{

        public TextView story;
        public TextView time;
        public TextView authorname;
        public TextView artistname;
        public TextView bookname;
        public ImageView image;
        public ImageView image1;
    }

    /****** Depends upon data size called for each row , Create each ListView row *****/
    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        ViewHolder holder;
        if (inflater == null) {
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if(vi==null){

            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
            vi = inflater.inflate(R.layout.customlist, null);

            /****** View Holder Object to contain tabitem.xml file elements ******/

            holder = new ViewHolder();
            holder.story = (TextView) vi.findViewById(R.id.storyname);
            holder.time=(TextView)vi.findViewById(R.id.time);
            holder.bookname=(TextView)vi.findViewById(R.id.bookname);
            holder.authorname = (TextView) vi.findViewById(R.id.authorname);
            holder.artistname=(TextView)vi.findViewById(R.id.artistname);
            holder.image=(ImageView)vi.findViewById(R.id.image);
            holder.image1=(ImageView)vi.findViewById(R.id.image1);
            /************  Set holder with LayoutInflater ************/
            vi.setTag( holder );
        }
        else
            holder=(ViewHolder)vi.getTag();

        if(data.size()<0)
        {
            holder.story.setText("No Data");
            holder.time.setVisibility(View.INVISIBLE);
        }
        else
        {
            /***** Get each Model object from Arraylist ********/
            tempValues=null;

            tempValues = ( StoryListModel) data.get( position );

            /************  Set Model values in Holder elements ***********/
            //Utility utility = new Utility();
            //File file = new File(String.valueOf(R.drawable.color_baloons));
            //Bitmap bm = utility.getOptimizedImageFile(file);
        //    Log.d(HomeActivity.LogTag, "tempValues: " + tempValues);
        //    Log.d(HomeActivity.LogTag, "data: " + data);
            String storyname = tempValues.getStoryname();
        /*    if(storyname.length()<=12){
                holder.story.setTextSize(30);
                holder.story.setText(storyname);
            }
            else  if(storyname.length()<=21 ) {
                {
                    holder.story.setTextSize(26);
                    holder.story.setText(storyname);

                }
            }

            else{

                holder.story.setTextSize(16);
                holder.story.setText(storyname);

            }*/

            holder.story.setTextSize(16);
            holder.story.setText(storyname);
            holder.time.setText("Duration : "+tempValues.getTimeduration());
            holder.authorname.setText(tempValues.getAuthorname());
            holder.artistname.setText(tempValues.getArtistname());
            holder.bookname.setText("Book : "+tempValues.getBookname());
            String authorId = tempValues.getAuthoreid();
            String artistid = tempValues.getArtistid();
            String authornewid="a"+authorId;
            String artistnewid="a"+artistid;
            Context context = holder.image.getContext();
            Context context1 = holder.image1.getContext();
            // int id = context.getResources().getIdentifier(newid, "drawable", context.getPackageName());
            int id = Utility.getImageById(authornewid, context);
            holder.image.setImageResource(id);
            int id1 = Utility.getImageById(artistnewid, context1);
            holder.image1.setImageResource(id1);
           // holder.image1.setImageBitmap(Utility.decodeSampledBitmapFromResource(activity.getResources(), id1, 70, 70));
            /******** Set Item Click Listner for LayoutInflater for each row *******/
        }
        return vi;
    }


}